package com.assignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DemoWebShopElectronics {

	public static void main(String[] args) throws InterruptedException {
		
		// Assignment 2
		
		 WebDriver driver = new ChromeDriver();
			
			driver.manage().window().maximize();
			
			driver.get("https://demowebshop.tricentis.com/login");
			
			driver.findElement(By.id("Email")).sendKeys("shopdemo1223@gmail.com");
			
			driver.findElement(By.id("Password")).sendKeys("Demo@123");
			
			driver.findElement(By.xpath("//input[@value='Log in']")).click();
			
			WebElement electronics = driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Electronics')]"));
			
			Actions act = new Actions(driver);
			
			act.moveToElement(electronics).perform();
			
			driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Cell phones')]")).click();
			
			WebElement dropdown = driver.findElement(By.id("products-orderby"));
			
			Select drop = new Select(dropdown);
			
			drop.selectByIndex(3);
			
			driver.findElement(By.xpath("(//input[@value='Add to cart'])[2]")).click();
			
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
			driver.findElement(By.xpath("//span[text()='Shopping cart']")).click();
			
			Thread.sleep(3000);
			
			driver.findElement(By.id("termsofservice")).click();
			
			driver.findElement(By.id("checkout")).click();
			
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("(//input[@title='Continue'])[1]")).click();
			
			driver.findElement(By.xpath("(//input[@title='Continue'])[2]")).click();
			
			driver.findElement(By.xpath("(//input[@value='Continue'])[3]")).click();
			
			driver.findElement(By.xpath("(//input[@value='Continue'])[4]")).click();
			
			driver.findElement(By.xpath("(//input[@value='Continue'])[5]")).click();
			
			driver.findElement(By.xpath("//input[@value='Confirm']")).click();
			
	        String status = driver.findElement(By.xpath("//strong[text()='Your order has been successfully processed!']")).getText();
			
			System.out.println(status);
			
			driver.findElement(By.xpath("//a[text()='Log out']")).click();
			driver.close();

	}

}
