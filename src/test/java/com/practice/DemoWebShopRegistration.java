package com.practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DemoWebShopRegistration {

	public static void main(String[] args) {
		
        WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://demowebshop.tricentis.com/register");
		
		driver.findElement(By.id("gender-male")).click();
		
		driver.findElement(By.id("FirstName")).sendKeys("Magudees");
		
		driver.findElement(By.id("LastName")).sendKeys("Waran");
		
		driver.findElement(By.id("Email")).sendKeys("shopdemo1223@gmail.com");
		
		driver.findElement(By.id("Password")).sendKeys("Demo@123");
		
		driver.findElement(By.id("ConfirmPassword")).sendKeys("Demo@123");
		
		driver.findElement(By.id("register-button")).click();
		
		String user = driver.findElement(By.linkText("shopdemo1223@gmail.com")).getText();
		
		System.out.println(user);

		

	}

}
